"use strict";

//Viergewinnt Objekt
function Game(mode, player, playercount, points, width, height, field, grav, win, winner, freefields) {
    this.mode = mode; //                    Spielmodus
    this.player = player; //                Aktueller Spieler
    this.playercount = playercount; //      Spielerzahl
    this.points = points; //                Punkteverteilung (Letzter Kettenbilder, Spielerpunkte 1-4)
    this.width = width; //                  Breite
    this.height = height; //                Höhe
    this.field = field; //                  Spielfeld (x, y, (Spieler, Feldbelegung 1-4))
    this.grav = grav; //                    Gravitation
    this.win = win; //                      Gewinnmodus
    this.winner = winner; //                Gewinner
    this.freefields = freefields; //        Anzahl freier Felder
}


//initialisiert
function initialize(mode, playercount, width, height, grav, win) {
    var player = 1;
    var winner = 0;
    var freefields = width * height;

    //Points initialisieren
    var points = new Array(5);
    for (var i = 0; i < 5; i++) {
        points[i] = 0;
    }

    //Spielfeld initialisieren
    var field = new Array(height);
    for (var i = 0; i < height; i++) {
        field[i] = new Array(width);
        for (var j = 0; j < width; j++) {
            field[i][j] = new Array(5);
            for (var k = 0; k < 5; k++) {
                field[i][j][k] = 0;
            }
        }
    }

    //Anfangs erreichbare Felder markieren
    if (grav == 0) {
        for (var i = 0; i < width; i++) {
            field[height - 1][i][0] = -1;
        }
    }
    if (grav == 1) {
        for (var i = 0; i < width; i++) {
            field[height - 1][i][0] = -1;
            field[0][i][0] = -1;
        }
        for (var i = 0; i < height; i++) {
            field[i][width - 1][0] = -1;
            field[i][0][0] = -1;
        }
    }

    var game = new Game(mode, player, playercount, points, width, height, field, grav, win, winner, freefields);
    return game;
}

//Führe einen Spielzug aus
Game.prototype.gamemove = function (x, y) {
    if (this.placeable(x, y)) {
        this.place(x, y);
        if (this.mode != 2) {
            this.check(x, y);
        }
        this.mark(x, y);
        return true;
    } else {
        return false;
    }
}

//setzt Spieler in Feld und berechnet nächsten Spieler
Game.prototype.place = function (x, y) {
    this.field[y][x][0] = this.player;
    this.freefields--;
    this.player = (this.player % this.playercount) + 1;
}

//prüft ob man in das gewählte Feld setzen darf
Game.prototype.placeable = function (x, y) {
    if (this.field[y][x][0] == -1) {
        return true;
    } else {
        return false;
    }
}

// noch innerhalb des Feldes?
Game.prototype.inField = function (x, y) {
    if (x >= 0 && x < this.width && y >= 0 && y < this.height) {
        return true;
    } else {
        return false;
    }
}

//setzt bei Ketten die Richtung der Kette
Game.prototype.drawCells = function (x, y, xdir, ydir, player, dir) {
    if (this.inField(x, y)) {
        // wenn gleiche Farbe wie Spieler, dann Richtung ergänzen und in gleiche Richtung weitersuchen
        if (this.field[y][x][0] == player) {
            this.field[y][x][dir] = 1;
            this.drawCells(x + xdir, y + ydir, xdir, ydir, player, dir);
        }
    }
}

//zählt zusammenhängende Kettenlänge
Game.prototype.countCells = function (x, y, xdir, ydir, player) {
    if (this.inField(x, y)) {
        // wenn gleiche Farbe wie Spieler, dann eins addieren und in gleiche Richtung weitersuchen
        if (this.field[y][x][0] == player) {
            return this.countCells(x + xdir, y + ydir, xdir, ydir, player) + 1;
        }
    }
    // außerhalb oder andere Farbe
    return 0;
}

//prüft auf Viererketten und legt Gewinner fest
Game.prototype.check = function (x, y) {
    var player = this.field[y][x][0];

    //horiontal
    if (this.countCells(x - 1, y, -1, 0, player) + this.countCells(x + 1, y, 1, 0, player) >= 3) {
        if (this.field[y][x][1] == 0) {
            this.points[0] = player;
            this.points[player]++;
            this.field[y][x][1] = 1;
        }
        this.drawCells(x - 1, y, -1, 0, player, 1);
        this.drawCells(x + 1, y, 1, 0, player, 1);
    }
    //diagonal (links-unten, rechts-oben)
    if (this.countCells(x - 1, y + 1, -1, 1, player) + this.countCells(x + 1, y - 1, 1, -1, player) >= 3) {
        if (this.field[y][x][2] == 0) {
            this.points[0] = player;
            this.points[player]++;
            this.field[y][x][2] = 1;
        }
        this.drawCells(x - 1, y + 1, -1, 1, player, 2);
        this.drawCells(x + 1, y - 1, 1, -1, player, 2);
    }
    //vertikal
    if (this.countCells(x, y - 1, 0, -1, player) + this.countCells(x, y + 1, 0, 1, player) >= 3) {
        if (this.field[y][x][3] == 0) {
            this.points[0] = player;
            this.points[player]++;
            this.field[y][x][3] = 1;
        }
        this.drawCells(x, y - 1, 0, -1, player, 3);
        this.drawCells(x, y + 1, 0, 1, player, 3);
    }
    //diagonal (links-oben, rechts-unten)
    if (this.countCells(x - 1, y - 1, -1, -1, player) + this.countCells(x + 1, y + 1, 1, 1, player) >= 3) {
        if (this.field[y][x][4] == 0) {
            this.points[0] = player;
            this.points[player]++;
            this.field[y][x][4] = 1;
        }
        this.drawCells(x - 1, y - 1, -1, -1, player, 4);
        this.drawCells(x + 1, y + 1, 1, 1, player, 4);
    }

    //Gewinner festlegen
    if (this.freefields == 0 || (this.win == 0 && this.points[0] != 0)) {
        var max = 0;
        for (var i = 1; i <= this.playercount; i++) {
            if (max < this.points[i]) {
                max = this.points[i];
                this.winner = i;
            }
        }
        //Unentschieden erkennen
        for (var i = 1; i <= this.playercount; i++) {
            if (max == this.points[i] && i != this.winner) {
                this.winner = 5;
            }
        }
    }
}

//gibt zurück ob Spielrand in bestimmte Richtung erreichbar
Game.prototype.reachBorder = function (x, y, xdir, ydir) {
    if (this.inField(x, y)) {
        if (this.field[y][x][0] > 0) {
            return false;
        }
        return this.reachBorder(x + xdir, y + ydir, xdir, ydir);
    } else {
        return true;
    }
}

//markiert Zellen die erreichbar/unerreichbar sind
Game.prototype.walkCells = function (x, y, xdir, ydir) {
    if (this.inField(x, y)) {
        if (this.field[y][x][0] <= 0) {
            this.field[y][x][0] = 5;

            //Nach oben
            if (this.reachBorder(x, y + 1, 0, 1)) {
                if (y > 0 && this.field[y - 1][x][0] > 0 || y == 0) {
                    this.field[y][x][0] = -1;
                }
                if (this.field[y][x][0] != -1) {
                    this.field[y][x][0] = 0;
                }
            }
            //Nach unten
            if (this.reachBorder(x, y - 1, 0, -1)) {
                if (y < this.height - 1 && this.field[y + 1][x][0] > 0 || y == this.height - 1) {
                    this.field[y][x][0] = -1;
                }
                if (this.field[y][x][0] != -1) {
                    this.field[y][x][0] = 0;
                }
            }
            //Nach rechts
            if (this.reachBorder(x + 1, y, 1, 0)) {
                if (x > 0 && this.field[y][x - 1][0] > 0 || x == 0) {
                    this.field[y][x][0] = -1;
                }
                if (this.field[y][x][0] != -1) {
                    this.field[y][x][0] = 0;
                }
            }
            //Nach links
            if (this.reachBorder(x - 1, y, -1, 0)) {
                if (x < this.width - 1 && this.field[y][x + 1][0] > 0 || x == this.width - 1) {
                    this.field[y][x][0] = -1;
                }
                if (this.field[y][x][0] != -1) {
                    this.field[y][x][0] = 0;
                }
            }

            //Freie Felder aktualisieren falls Felder nicht erreichbar
            if (this.field[y][x][0] == 5) {
                this.freefields--;
            }

            this.walkCells(x + xdir, y + ydir, xdir, ydir);
        }
    }
}

//Funktion für den Flip-Modus der das Feld im Uhrzeigersinn dreht und danach alle Steine herunterfallen lässt	
Game.prototype.flipfall = function () {
    //Hilfsarrays initialisieren
    var flip_field = new Array(this.height);
    var fall_field = new Array(this.height);
    for (var i = 0; i < this.height; i++) {
        flip_field[i] = new Array(this.width);
        fall_field[i] = new Array(this.width);
        for (var j = 0; j < this.width; j++) {
            flip_field[i][j] = new Array(5);
            fall_field[i][j] = new Array(5);
            for (var k = 0; k < 5; k++) {
                flip_field[i][j][k] = 0;
                fall_field[i][j][k] = 0;
            }
        }
    }

    //dreht das Feld im Uhrzeigersinn
    for (var i = 0; i < this.height; i++) {
        for (var j = 0; j < this.width; j++) {
            for (var k = 0; k < 5; k++) {
                flip_field[j][(this.height - 1 - i)][k] = this.field[i][j][k];
            }
        }
    }

    //lässt Steine runterfallen
    for (var i = 0; i < this.width; i++) {
        for (var j = this.height - 1, p = this.height - 1; j >= 0; j--) {
            if (flip_field[j][i][0] > 0) {
                for (var k = 0; k < 5; k++) {
                    fall_field[p][i][k] = flip_field[j][i][k];
                }
                p--;
            }
        }
        for (; p >= 0; p--) {
            for (var k = 0; k < 5; k++) {
                fall_field[p][i][k] = 0;
            }
        }
    }

    this.field = fall_field;

    //prüft neue Konstellation auf Ketten
    for (var i = 0; i < this.height; i++) {
        for (var j = 0; j < this.width; j++) {
            if (this.field[i][j][0] > 0) {
                this.check(j, i);
            }
        }
    }
}

//aktualisiert erreichbare Felder
Game.prototype.mark = function (x, y) {
    if (this.mode != 2 && this.grav == 0) {
        //Feld drüber nun erreichbar
        if (y > 0) {
            this.field[y - 1][x][0] = -1;
        }
    }

    if (this.mode != 2 && this.grav == 1) {
        //in allen Richtungen überprüfen welche Felder noch erreichbar sind
        this.walkCells(x, y + 1, 0, 1);
        this.walkCells(x - 1, y, -1, 0);
        this.walkCells(x, y - 1, 0, -1);
        this.walkCells(x + 1, y, 1, 0);
    }

    if (this.mode == 2) {
        //Feld drehen und fallen lassen
        this.flipfall();

        //alte Erreichbarkeiten zurücksetzen
        for (var i = 0; i < this.height; i++) {
            for (var j = 0; j < this.width; j++) {
                if (this.field[i][j][0] == -1) {
                    this.field[i][j][0] = 0;
                }
            }
        }

        //neue Erreichbarkeiten einfügen
        for (var i = 0; i < this.width; i++) {
            for (var j = 0; j < this.height; j++) {
                if (this.field[j][i][0] == 0) {
                    if ((j < this.height - 1 && this.field[j + 1][i][0] > 0) || j == this.height - 1) {
                        this.field[j][i][0] = -1;
                        break;
                    }
                } else {
                    break;
                }
            }
        }
    }
}

//Gibt eine Menge aller möglichen Spielzüge zurück
Game.prototype.possibilities = function () {
    var array = new Array();
    for (var i = 0; i < this.width; i++) {
        for (var j = 0; j < this.height; j++) {
            if (this.field[j][i][0] == -1) {
                array.push(new Array(i, j));
            }
        }
    }

    //Zufällig sortieren
    for (var i = array.length; i > 0; i--) {
        var j = Math.floor(Math.random() * i);
        var t = array[i - 1];
        array[i - 1] = array[j];
        array[j] = t;
    }

    return array;
}

//Erstellt eine Kopie des Objekts
Game.prototype.copy = function () {
    var copy = new Game(0, 0, 0, [], 0, 0, [], 0, 0, 0, 0);
    copy.mode = this.mode;
    copy.player = this.player;
    copy.playercount = this.playercount;
    copy.width = this.width;
    copy.height = this.height;
    copy.grav = this.grav;
    copy.win = this.win;
    copy.winner = this.winner;
    copy.points = this.points.slice();

    copy.field = new Array(copy.height);
    for (var i = 0; i < copy.height; i++) {
        copy.field[i] = new Array(copy.width);
        for (var j = 0; j < copy.width; j++) {
            copy.field[i][j] = this.field[i][j].slice();
        }
    }

    return copy;
}