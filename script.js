var game; //Spielinstanz
var round; //Fortschritt der Spielrunde

//Spielfeld ausgeben
function draw(field, width, height, player) {
	$("#field").empty();

	//Aufbau des Feldes
	for (var i = 0; i < height; i++) {
		$("#field").append('<div class="stonerow"></div>');
		for (var j = 0; j < width; j++) {
			//Farbige Spielsteine
			if (field[i][j][0] == -1) {
				$(".stonerow").eq(i).append('<div class="stone white clickable"></div>');
			}
			if (field[i][j][0] == 0) {
				$(".stonerow").eq(i).append('<div class="stone white"></div>');
			}
			if (field[i][j][0] == 1) {
				$(".stonerow").eq(i).append('<div class="stone blue"></div>');
			}
			if (field[i][j][0] == 2) {
				$(".stonerow").eq(i).append('<div class="stone red"></div>');
			}
			if (field[i][j][0] == 3) {
				$(".stonerow").eq(i).append('<div class="stone green"></div>');
			}
			if (field[i][j][0] == 4) {
				$(".stonerow").eq(i).append('<div class="stone yellow"></div>');
			}
			if (field[i][j][0] == 5) {
				$(".stonerow").eq(i).append('<div class="stone gray"></div>');
			}

			//Markierung von Viererketten
			if (field[i][j][0] > 0 && field[i][j][0] < 5) {
				if (field[i][j][1] == 1) {
					$(".stonerow").eq(i).children().eq(j).append('<image class="img" src="resources/side.png"></image>');
				}
				if (field[i][j][2] == 1) {
					$(".stonerow").eq(i).children().eq(j).append('<image class="img" src="resources/right.png"></image>');
				}
				if (field[i][j][3] == 1) {
					$(".stonerow").eq(i).children().eq(j).append('<image class="img" src="resources/top.png"></image>');
				}
				if (field[i][j][4] == 1) {
					$(".stonerow").eq(i).children().eq(j).append('<image class="img" src="resources/left.png"></image>');
				}
			}
		}
	}

	//Größe und Ränder der Spielsteine	
	var min = Math.floor(((($("#field").width()) / width) < (($("#field").height()) / height)) ? (($("#field").width()) / width) : (($("#field").height()) / height));
	var shadow = Math.floor(min / 10);
	var size = min - shadow;
	$(".stone").width(size);
	$(".stone").height(size);
	$(".stone").css("margin", Math.floor(shadow / 2));
	$(".stone.white").css("box-shadow", "0px 0px 0px " + shadow / 4 + "px white inset, 0px 0px 0px " + shadow + "px black inset, " + shadow / 2 + "px " + shadow / 2 + "px " + shadow * 4 + "px 0px black inset");
	$(".stone.gray").css("box-shadow", "0px 0px 0px " + shadow / 4 + "px #111111 inset, 0px 0px 0px " + shadow + "px black inset, " + shadow / 2 + "px " + shadow / 2 + "px " + shadow * 4 + "px 0px black inset");
	$(".stone.blue").css("box-shadow", "0px 0px 0px " + shadow / 4 + "px blue inset, 0px 0px 0px " + shadow + "px black inset, " + shadow / 2 + "px " + shadow / 2 + "px " + shadow * 4 + "px 0px black inset");
	$(".stone.red").css("box-shadow", "0px 0px 0px " + shadow / 4 + "px red inset, 0px 0px 0px " + shadow + "px black inset, " + shadow / 2 + "px " + shadow / 2 + "px " + shadow * 4 + "px 0px black inset");
	$(".stone.green").css("box-shadow", "0px 0px 0px " + shadow / 4 + "px green inset, 0px 0px 0px " + shadow + "px black inset, " + shadow / 2 + "px " + shadow / 2 + "px " + shadow * 4 + "px 0px black inset");
	$(".stone.yellow").css("box-shadow", "0px 0px 0px " + shadow / 4 + "px yellow inset, 0px 0px 0px " + shadow + "px black inset, " + shadow / 2 + "px " + shadow / 2 + "px " + shadow * 4 + "px 0px black inset");

	//Hover der klickbaren Spielsteine
	if (player == 1) {
		$(".clickable").hover(function () {
				$(this).css("background-color", "#7777FF");
				$(this).css("box-shadow", "0px 0px 0px " + shadow / 4 + "px #7777FF inset, 0px 0px 0px " + shadow + "px black inset, " + shadow / 2 + "px " + shadow / 2 + "px " + shadow * 4 + "px 0px black inset");
			},
			function () {
				$(this).css("background-color", "#FFFFFF");
				$(this).css("box-shadow", "0px 0px 0px " + shadow / 4 + "px #FFFFFF inset, 0px 0px 0px " + shadow + "px black inset, " + shadow / 2 + "px " + shadow / 2 + "px " + shadow * 4 + "px 0px black inset");
			});
	}
	if (player == 2) {
		$(".clickable").hover(function () {
				$(this).css("background-color", "#FF7777");
				$(this).css("box-shadow", "0px 0px 0px " + shadow / 4 + "px #FF7777 inset, 0px 0px 0px " + shadow + "px black inset, " + shadow / 2 + "px " + shadow / 2 + "px " + shadow * 4 + "px 0px black inset");
			},
			function () {
				$(this).css("background-color", "#FFFFFF");
				$(this).css("box-shadow", "0px 0px 0px " + shadow / 4 + "px #FFFFFF inset, 0px 0px 0px " + shadow + "px black inset, " + shadow / 2 + "px " + shadow / 2 + "px " + shadow * 4 + "px 0px black inset");
			});
	}
	if (player == 3) {
		$(".clickable").hover(function () {
				$(this).css("background-color", "#77FF77");
				$(this).css("box-shadow", "0px 0px 0px " + shadow / 4 + "px #77FF77 inset, 0px 0px 0px " + shadow + "px black inset, " + shadow / 2 + "px " + shadow / 2 + "px " + shadow * 4 + "px 0px black inset");
			},
			function () {
				$(this).css("background-color", "#FFFFFF");
				$(this).css("box-shadow", "0px 0px 0px " + shadow / 4 + "px #FFFFFF inset, 0px 0px 0px " + shadow + "px black inset, " + shadow / 2 + "px " + shadow / 2 + "px " + shadow * 4 + "px 0px black inset");
			});
	}
	if (player == 4) {
		$(".clickable").hover(function () {
				$(this).css("background-color", "#FFFF77");
				$(this).css("box-shadow", "0px 0px 0px " + shadow / 4 + "px #FFFF77 inset, 0px 0px 0px " + shadow + "px black inset, " + shadow / 2 + "px " + shadow / 2 + "px " + shadow * 4 + "px 0px black inset");
			},
			function () {
				$(this).css("background-color", "#FFFFFF");
				$(this).css("box-shadow", "0px 0px 0px " + shadow / 4 + "px #FFFFFF inset, 0px 0px 0px " + shadow + "px black inset, " + shadow / 2 + "px " + shadow / 2 + "px " + shadow * 4 + "px 0px black inset");
			});
	}
}

//Startet das Spiel mit den eingestellten Werten
$("#los").click(function () {
	//Passende Seiten einblenden/ausblenden
	$("#menu").css("display", "none");
	$("#infos").css("display", "none");
	$("#game").css("display", "block");

	var mode = $(".radiobutton[name='mode'].active").attr("value");
	var player = parseInt($("#player").val()) + parseInt($("#computer").val());
	var width = $("#width").val();
	var height = $("#height").val();
	var grav = $(".radiobutton[name='grav'].active").attr("value");
	var win = $(".radiobutton[name='win'].active").attr("value");

	game = initialize(mode, player, width, height, grav, win);
	round = 0;
	displaymove(game);

	//Adsense Werbeanfrage
	(adsbygoogle = window.adsbygoogle || []).push({});
	(adsbygoogle = window.adsbygoogle || []).push({});
});

function displaymove(game) {
	draw(game.field, game.width, game.height, game.player);

	//Punkte der Spieler anzeigen
	$(".points").html("<br>");
	if ($(".radiobutton[name='win'].active").attr("value") == 1) {
		if (game.playercount >= 1) {
			$("#bluepoints").html("Blau:&nbsp;" + game.points[1]);
		}
		if (game.playercount >= 2) {
			$("#redpoints").html("Rot:&nbsp;" + game.points[2]);
		}
		if (game.playercount >= 3) {
			$("#greenpoints").html("Gruen:&nbsp;" + game.points[3]);
		}
		if (game.playercount >= 4) {
			$("#yellowpoints").html("Gelb:&nbsp;" + game.points[4]);
		}
	}

	//Gewinn anzeigen
	if (game.winner != 0) {

		$("#win").css("display", "block");
		var message;
		if (game.winner == 1) {
			message = "Spieler Blau hat mit " + game.points[game.winner] + "&nbsp;Punkten gewonnen";
		}
		if (game.winner == 2) {
			message = "Spieler Rot hat mit " + game.points[game.winner] + "&nbsp;Punkten gewonnen";
		}
		if (game.winner == 3) {
			message = "Spieler Gruen hat mit " + game.points[game.winner] + "&nbsp;Punkten gewonnen";
		}
		if (game.winner == 4) {
			message = "Spieler Gelb hat mit " + game.points[game.winner] + "&nbsp;Punkten gewonnen";
		}
		if (game.winner == 5) {
			message = "Es gibt ein Unentschieden";
		}
		$("#wintext").html(message);
	}
}

//Ein Stein im Spielfeld wurde gedrückt
function stoneclick() {
	var x = $(this).index();
	var y = $(this).parent().index();
	var valid = game.gamemove(x, y);
	if (valid) {
		$(document).off("click", ".stone", stoneclick);
		displaymove(game);
		round = (round + 1) % game.playercount;

		var intervalnumber = setInterval(function () {
			var i = 0;
			if ($("#computer").val() - i > 0 && round >= $("#player").val() && game.winner == 0) {
				maketurn(game, $("#strength").val());
				displaymove(game);
				round = (round + 1) % game.playercount;
				i++;
			}
			if ($("#computer").val() - i == 0 || round < $("#player").val() || game.winner != 0) {
				$(document).on("click", ".stone", stoneclick);
				clearInterval(intervalnumber);
			}
		}, 100);
	}
}
$(document).on("click", ".stone", stoneclick);

//Auf die Gewinnmitteilung wurde geklickt
$("#win").click(function () {
	//Zum Startmenü zurück
	$("#menu").css("display", "block");
	$("#infos").css("display", "block");
	$("#game").css("display", "none");
	$("#field").empty();
	$("#win").css("display", "none");
});

//Radiobutton wechseln
$(".radiobutton").click(function () {
	if (!$(this).hasClass("disabled")) {
		$(".radiobutton[name=" + $(this).attr("name") + "]").removeClass("active");
		$(this).addClass("active");
	}
});

//Aktualisiert die Benutzbarkeit von Inputfeldern bei der Auswahl von verschiedenen Spielmodi
$(".radiobutton[name='mode']").click(function () {
	if ($(".radiobutton[name='mode'].active").attr("value") == 0) {
		//Felder deaktivieren
		$(".radiobutton[name='grav']").addClass("disabled");
		$(".radiobutton[name='win']").addClass("disabled");
		$("#player").prop("max", 2);
		$("#computer").prop("max", 1);
		$("#strength").prop("max", 8);
		$("#player, #computer, #strength").rangeslider('update', true);
		$("#width+.rangeslider").addClass("rangeslider--disabled");
		$("#height+.rangeslider").addClass("rangeslider--disabled");
		//Werte zurücksetzen
		$("#player").val(1).change();
		$("#computer").val(1).change();
		$("#strength").val(6).change();
		$(".radiobutton[name='grav'][value=0]").addClass("active");
		$(".radiobutton[name='grav'][value=1]").removeClass("active");
		$("#width").val(7).change();
		$("#height").val(6).change();
		$(".radiobutton[name='win'][value=0]").addClass("active");
		$(".radiobutton[name='win'][value=1]").removeClass("active");
	}
	if ($(".radiobutton[name='mode'].active").attr("value") == 1) {
		//Felder aktivieren
		$(".radiobutton[name='grav']").removeClass("disabled");
		$(".radiobutton[name='win']").removeClass("disabled");
		$("#player").prop("max", 4);
		$("#computer").prop("max", 3);
		$("#strength").prop("max", 4);
		$("#player, #computer, #strength").rangeslider('update', true);
		$("#width+.rangeslider").removeClass("rangeslider--disabled");
		$("#height+.rangeslider").removeClass("rangeslider--disabled");
		//Werte zurücksetzen
		$("#player").val(2).change();
		$("#computer").val(0).change();
		$("#strength").val(3).change();
		$(".radiobutton[name='grav'][value=0]").addClass("active");
		$(".radiobutton[name='grav'][value=1]").removeClass("active");
		$("#width").val(7).change();
		$("#height").val(6).change();
		$(".radiobutton[name='win'][value=0]").addClass("active");
		$(".radiobutton[name='win'][value=1]").removeClass("active");
	}
	if ($(".radiobutton[name='mode'].active").attr("value") == 2) {
		//Felder aktivieren
		$(".radiobutton[name='grav']").addClass("disabled");
		$(".radiobutton[name='win']").addClass("disabled");
		$("#player").prop("max", 2);
		$("#computer").prop("max", 1);
		$("#strength").prop("max", 8);
		$("#player, #computer, #strength").rangeslider('update', true);
		$("#width+.rangeslider").removeClass("rangeslider--disabled");
		$("#height+.rangeslider").removeClass("rangeslider--disabled");
		//Werte zurücksetzen
		$("#player").val(1).change();
		$("#computer").val(1).change();
		$("#strength").val(6).change();
		$(".radiobutton[name='grav'][value=0]").addClass("active");
		$(".radiobutton[name='grav'][value=1]").removeClass("active");
		$("#width").val(6).change();
		$("#height").val(6).change();
		$(".radiobutton[name='win'][value=0]").addClass("active");
		$(".radiobutton[name='win'][value=1]").removeClass("active");
	}
});

//Bei Flip-Modus Spielfeldgröße quadratisch halten
$("#width").on("input", function () {
	if ($(".radiobutton[name='mode'].active").attr("value") == 2) {
		$("#height").val($("#width").val());
		$("#height").rangeslider('update', true);
	}
});
$("#height").on("input", function () {
	if ($(".radiobutton[name='mode'].active").attr("value") == 2) {
		$("#width").val($("#height").val());
		$("#width").rangeslider('update', true);
	}
});

//Bei Spielerzahl passend festlegen
$("#player").on("input", function () {
	if (parseInt($("#player").val()) + parseInt($("#computer").val()) > $("#player").prop("max")) {
		$("#computer").val($("#player").prop("max") - $("#player").val());
		$("#computer").rangeslider('update', true);
	}
	if (parseInt($("#player").val()) + parseInt($("#computer").val()) < $("#player").prop("max") && $(".radiobutton[name='mode'].active").attr("value") != 1) {
		$("#computer").val($("#player").prop("max") - $("#player").val());
		$("#computer").rangeslider('update', true);
	}
	if ($(".radiobutton[name='mode'].active").attr("value") == 1 && $("#computer").val() == 0) {
		$(".radiobutton[name='win']").removeClass("disabled");
	}
});
$("#computer").on("input", function () {
	if (parseInt($("#player").val()) + parseInt($("#computer").val()) > $("#player").prop("max")) {
		$("#player").val($("#player").prop("max") - $("#computer").val());
		$("#player").rangeslider('update', true);
	}
	if (parseInt($("#player").val()) + parseInt($("#computer").val()) < $("#player").prop("max") && $(".radiobutton[name='mode'].active").attr("value") != 1) {
		$("#player").val($("#player").prop("max") - $("#computer").val());
		$("#player").rangeslider('update', true);
	}
	if ($(".radiobutton[name='mode'].active").attr("value") == 1) {
		if (parseInt($("#computer").val()) > 0) {
			$(".radiobutton[name='win']").addClass("disabled");
			$(".radiobutton[name='win'][value=0]").addClass("active");
			$(".radiobutton[name='win'][value=1]").removeClass("active");
		} else {
			$(".radiobutton[name='win']").removeClass("disabled");
		}
	}
});

//Spielstein Anpassung bei Fenstergrößen Änderung
$(window).resize(function () {
	var width = $("#width").val();
	var height = $("#height").val();
	var min = Math.floor(((($("#field").width()) / width) < (($("#field").height()) / height)) ? (($("#field").width()) / width) : (($("#field").height()) / height));
	var shadow = Math.floor(min / 10);
	var size = min - shadow;

	$(".stone").width(size);
	$(".stone").height(size);
	$(".stone").css("margin", Math.floor(shadow / 2));
	$(".stone.white").css("box-shadow", "0px 0px 0px " + shadow / 4 + "px white inset, 0px 0px 0px " + shadow + "px black inset, " + shadow / 2 + "px " + shadow / 2 + "px " + shadow * 4 + "px 0px black inset");
	$(".stone.gray").css("box-shadow", "0px 0px 0px " + shadow / 4 + "px #111111 inset, 0px 0px 0px " + shadow + "px black inset, " + shadow / 2 + "px " + shadow / 2 + "px " + shadow * 4 + "px 0px black inset");
	$(".stone.blue").css("box-shadow", "0px 0px 0px " + shadow / 4 + "px blue inset, 0px 0px 0px " + shadow + "px black inset, " + shadow / 2 + "px " + shadow / 2 + "px " + shadow * 4 + "px 0px black inset");
	$(".stone.red").css("box-shadow", "0px 0px 0px " + shadow / 4 + "px red inset, 0px 0px 0px " + shadow + "px black inset, " + shadow / 2 + "px " + shadow / 2 + "px " + shadow * 4 + "px 0px black inset");
	$(".stone.green").css("box-shadow", "0px 0px 0px " + shadow / 4 + "px green inset, 0px 0px 0px " + shadow + "px black inset, " + shadow / 2 + "px " + shadow / 2 + "px " + shadow * 4 + "px 0px black inset");
	$(".stone.yellow").css("box-shadow", "0px 0px 0px " + shadow / 4 + "px yellow inset, 0px 0px 0px " + shadow + "px black inset, " + shadow / 2 + "px " + shadow / 2 + "px " + shadow * 4 + "px 0px black inset");
});

//Felder Initialisieren
$(document).ready(function () {
	$("#width+.rangeslider").addClass("rangeslider--disabled");
	$("#height+.rangeslider").addClass("rangeslider--disabled");
});

//Rangeslider Funktionen
$(function () {
	var $element = $('[data-rangeslider]');

	// Basic rangeslider initialization
	$element.rangeslider({
		// Deactivate the feature detection
		polyfill: false,
		// Callback function
		onInit: function () {
			this.$element[0].parentNode.getElementsByClassName('rangeslider__handle')[0]['innerText'] = this.$element[0]
				.value;
		},

		// Callback function
		onSlide: function (position, value) {
			this.$element[0].parentNode.getElementsByClassName('rangeslider__handle')[0]['innerText'] = this.$element[0]
				.value;
		}
	});
});