"use strict";

var newmove, maxdepth;

//Führt Spielzug aus
function maketurn(game, depth) {
    maxdepth = parseInt(depth);
    newmove = game.possibilities().pop();

    //Auswahl und Start des passenden Algorithmus
    if (game.playercount == 2) {
        minimax(game, game.player, maxdepth, Number.NEGATIVE_INFINITY, Number.POSITIVE_INFINITY);
    } else {
        var alpha = new Array(game.playercount);
        for (var i = 0; i < alpha.length; i++) {
            alpha[i] = Number.NEGATIVE_INFINITY;
        }
        hypermax(game, game.player, maxdepth, alpha);
    }

    game.gamemove(newmove[0], newmove[1]);
    console.log(" === ");
}

//Sucht den besten Zug bei zwei Spielern aus
function minimax(game, player, depth, alpha, beta) {
    var zuege = game.possibilities();

    if (depth == 0 || zuege.length == 0 || game.winner != 0) {
        return -bewerten(game, player, depth);
    }

    zuege = ordermoves(zuege, game);
    var max = alpha;
    while (zuege.length > 0) {
        var copy = game.copy();
        var zug = zuege.pop();
        copy.gamemove(zug[0], zug[1]);

        var wert = -minimax(copy, game.player, depth - 1, -beta, -max);

        if (depth == maxdepth) {
            console.log("Wertung: " + wert + " - Spalte: " + zug[0] + " - Zeile: " + zug[1]);
        }

        if (wert > max) {
            max = wert;
            if (max >= beta) {
                break;
            }
            if (depth == maxdepth) {
                newmove = zug.slice();
            }
        }
    }
    return max;
}

//Bewertet den jetzigen Spielstand
function bewerten(game, player, depth) {
    var wert = 0;
    if (game.winner == player) {
        wert = depth + 1;
    }
    return wert;
}

//Sortiert die Zugmöglichkeiten um bessere Ergebnisse zu bekommen
function ordermoves(zuege, game) {
    var array = new Array();

    for (var zug of zuege) {
        //Feld-Innerere Positionen besser bewerten
        var gausx = 1 / (Math.pow(Math.PI, 0.5) * (game.width - 1) / 4) * Math.pow(Math.E, -0.5 * Math.pow((zug[0] - (game.width - 1) / 2) / ((game.width - 1) / 4), 2));
        var gausy = 1 / (Math.pow(Math.PI, 0.5) * (game.height - 1) / 4) * Math.pow(Math.E, -0.5 * Math.pow((zug[1] - (game.height - 1) / 2) / ((game.height - 1) / 4), 2));
        var gaus = Math.pow(Math.pow(gausx, 2) + Math.pow(gausy, 2), 0.5);

        //Zeilen nach Spielernummer bewerten
        var zeile = 1;
        if (game.win == 1) {
            zeile = ((zug[1] + 1) % 2 == (game.player + 1) % 2) ? 1 : (1 - 1 / game.height);
        }

        let wert = gaus * zeile;
        array.push([wert, zug.slice()]);
    }

    array.sort(function (a, b) {
        return a[0] - b[0];
    });

    zuege = new Array();
    for (var i = 0; i < array.length; i++) {
        zuege.push(array[i][1].slice());
    }

    return zuege;
}

//Sucht den besten Zug bei mehr als zwei Spielern aus
function hypermax(game, player, depth, alpha) {
    var zuege = game.possibilities();

    if (depth == 0 || zuege.length == 0 || game.winner != 0) {
        return hyperbewerten(game, depth);
    }

    zuege = ordermoves(zuege, game);
    var max = [];
    var first = true;
    while (zuege.length > 0) {
        var copy = game.copy();
        var zug = zuege.pop();
        copy.gamemove(zug[0], zug[1]);

        var wert = hypermax(copy, copy.player, depth - 1, alpha.slice());

        if (depth == maxdepth) {
            console.log("Wertung: " + wert + " - Spalte: " + zug[0] + " - Zeile: " + zug[1]);
        }

        if (first) {
            max = wert;
            first = false;
            if (depth == maxdepth) {
                newmove = zug.slice();
            }
        }

        if (alpha[player - 1] < wert[player - 1]) {
            alpha[player - 1] = wert[player - 1];
            max = wert;
            if (depth == maxdepth) {
                newmove = zug.slice();
            }
        }

        if (alpha.reduce((a, b) => a + b, 0) >= 0) {
            break;
        }

    }
    return max;
}

//Bewertet den jetzigen Spielstand für alle Spieler
function hyperbewerten(game, depth) {
    var wert = new Array(game.playercount);
    for (var i = 0; i < wert.length; i++) {
        wert[i] = bewerten(game, i + 1, depth);
        for (var j = 0; j < game.playercount; j++) {
            wert[i] = wert[i] - (1 / game.playercount) * bewerten(game, j + 1, depth);
        }
    }
    return wert;
}